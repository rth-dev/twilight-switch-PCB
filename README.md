# twilight-switch
controller board intended to do something a very very long time, depending on ambient light. low power consumption is rule!

* Schematic Rev B - [pdf](https://gitlab.com/rth-dev/twilight-switch-PCB/-/raw/master/revisions/Rev_B/twilight%20switch%20Rev_B.pdf)

![Rev B front](https://gitlab.com/rth-dev/twilight-switch-PCB/-/raw/master/revisions/Rev_B/twilight%20switch%20Rev_B%20front.png)

![Rev B back](https://gitlab.com/rth-dev/twilight-switch-PCB/-/raw/master/revisions/Rev_B/twilight%20switch%20Rev_B%20back.png)